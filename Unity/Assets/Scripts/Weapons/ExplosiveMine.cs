﻿using UnityEngine;
using System.Collections;

public class ExplosiveMine : MonoBehaviour {

	public float detectionRadius = 10.0f;
	public float explodeAtRadius = 5.0f;
	public float armedTimer = 3.0f;

	public string pooledExplosionPrefab;
	public Color armedLight;
	public Color detectedLight;
	 
	Light theLight;

	bool armed = false;
	float timer;

	// Use this for initialization
	void OnEnable () {
		theLight = GetComponentInChildren<Light> ();
		theLight.intensity = 0;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer >= armedTimer)
			armed = true;
	}

	void FixedUpdate() {
		if (armed) {
			theLight.color = armedLight;
			theLight.intensity = 2;

			Collider[] enemiesInRange = Physics.OverlapSphere (transform.position, detectionRadius);

			foreach (Collider e in enemiesInRange) {
				if (e.CompareTag ("Enemy")) {
					float dist = Vector3.Distance (transform.position, e.gameObject.transform.position);

					if (dist <= detectionRadius) {
						theLight.color = detectedLight;

						if (dist <= explodeAtRadius) {
							SpawnExplosion ();
							Destroy (gameObject);
						}
					}
				}
			}
		}
	}

	public void SpawnExplosion () {
		if (pooledExplosionPrefab != "") {
			GameObject g = PoolManager.current.GetPooledObject (pooledExplosionPrefab);
			
			if (g != null) {
				g.transform.position = transform.position;
				g.transform.rotation = transform.rotation;
				g.SetActive (true);
			}
		}
	}
}

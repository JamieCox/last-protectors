﻿using UnityEngine;
using System.Collections;

public class BaseAmmo : MonoBehaviour {

	//Things the base ammo needs to do:
	//Move (Duh) (Done)
	//Damage enemies (Done, I think)
	//Despawn after a certain ammo of time (Done)
	//Despawn if it hits an obstacle
	[Header("General Settings")]
	public int baseDamage = 15;
	public float velocity = 20.0f;
	private int damageVariation;

	public float destroyTime = 2.5f;

	public string damageTag = "";

	public string pooledImpactEffect;


	void OnEnable() {
		Invoke ("Destroy", destroyTime);
		damageVariation = baseDamage / 4;
	}

	void FixedUpdate() {
		GetComponent<Rigidbody>().velocity = transform.forward * velocity;
	}

	public virtual void OnTriggerEnter(Collider collider) {

		if (collider.CompareTag (damageTag)) {
			int dmg = Random.Range ((baseDamage - damageVariation), (baseDamage + damageVariation));     
			collider.SendMessage ("TakeDamage", dmg);
		}
		gameObject.SetActive (false);
		SpawnExplosion ();
	}


	public void SpawnExplosion () {
		if (pooledImpactEffect != "") {
			GameObject g = PoolManager.current.GetPooledObject(pooledImpactEffect);
			
			if (g != null) {
				g.transform.position = transform.position;
				g.transform.rotation = transform.rotation;
				g.SetActive(true);
			}
		}
	}

	void Destroy() {
		gameObject.SetActive (false);
	}

	void OnDisable() {
		CancelInvoke("Destroy");
	}
}

﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public float explosionDuration = 5f;
	public float explosionRadius = 8f;
	public int explosionDamageAtCentre = 65;
	public bool damagePlayer = false;

	void OnEnable() {
		if (isActiveAndEnabled)
			ExplosionDamage ();
	}

	void Update() {

		AnimateExplosion ();
		//Things left to do:
		//Make explosion hurt enemies.

	}

	void AnimateExplosion() {
		float r = Mathf.Sin((Time.time / explosionDuration) * (2 * Mathf.PI)) * 0.5f + 0.25f;
		float g = Mathf.Sin((Time.time / explosionDuration + 0.33333333f) * 2 * Mathf.PI) * 0.5f + 0.25f;
		float b = Mathf.Sin((Time.time / explosionDuration + 0.66666667f) * 2 * Mathf.PI) * 0.5f + 0.25f;
		float correction = 1 / (r + g + b);
		r *= correction;
		g *= correction;
		b *= correction;
		GetComponent<Renderer>().material.SetVector("_ChannelFactor", new Vector4(r,g,b,0));
	}

	void ExplosionDamage() {
		Collider[] hitColliders = Physics.OverlapSphere (transform.position, explosionRadius);

		foreach (Collider c in hitColliders) {
			if (c.CompareTag ("Enemy")) {
				float distance = Vector3.Distance(transform.position, c.transform.position);
				float proportionalDist = 1 - distance/explosionRadius;
				float damageToDeal = explosionDamageAtCentre * proportionalDist;
				c.SendMessage ("TakeDamage", damageToDeal);
			}

			if (c.CompareTag("Player")) {
				if (damagePlayer) {
					float distance = Vector3.Distance(transform.position, c.transform.position);
					float proportionalDist = 1 - distance/explosionRadius;
					float damageToDeal = explosionDamageAtCentre * proportionalDist;
					c.SendMessage ("TakeDamage", damageToDeal);
				}
				else {
					float damageToDeal = explosionRadius / 3;
					c.SendMessage("TakeDamage", damageToDeal);
				}
			}
		}
	}
}

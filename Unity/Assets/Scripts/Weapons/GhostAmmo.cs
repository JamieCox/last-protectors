﻿using UnityEngine;
using System.Collections;

public class GhostAmmo : BaseAmmo {
	
	public override void OnTriggerEnter(Collider collider) {
		if (collider.CompareTag ("Enemy")) {
			collider.SendMessage ("TakeDamage", baseDamage);
		} else {
			gameObject.SetActive(false);
			SpawnExplosion();
		}
	}
	
}

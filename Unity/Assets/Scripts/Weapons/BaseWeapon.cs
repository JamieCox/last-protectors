﻿using UnityEngine;
using System.Collections;

public class BaseWeapon : MonoBehaviour {
	[Header("Weapon Ammunition")]
	public string pooledAmmoType = "";
	public string pooledShell = "";

	[Header("Weapon Spawn Points")]
	public Transform[] bulletSpawnPoint;
	public Transform shellSpawnPoint;

	[Header("Weapon Settings")]
	public float timeBetweenShots = 0.4f;
	public static float timer;

	public static Light muzzleLight;
	public static float effectDisplayLength = 0.08f;

	void Awake() {
		muzzleLight = GetComponentInChildren<Light> ();
	}

	void Update() {
		timer += Time.deltaTime;

		if (Input.GetButton ("Fire1") && timer >= timeBetweenShots) {
			Shoot();
		}

		if (timer >= timeBetweenShots * effectDisplayLength) {
			DisableEffects();
		}
	}

	public virtual void Shoot() {
		timer = 0.0f;

		muzzleLight.enabled = true;

		foreach (Transform point in bulletSpawnPoint)
			Spawn (point, pooledAmmoType);

		Spawn (shellSpawnPoint, pooledShell);
		

	}

	public void DisableEffects() {
		muzzleLight.enabled = false;
	}

	public void Spawn(Transform pTransform, string objName) {
		if (objName != "") {
			GameObject g = PoolManager.current.GetPooledObject(objName);

			if (g != null) {
				g.transform.position = pTransform.position;
				g.transform.rotation = pTransform.rotation;
				g.SetActive(true);
			}
		}
	}
}

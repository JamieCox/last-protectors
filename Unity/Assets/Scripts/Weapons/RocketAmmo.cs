﻿using UnityEngine;
using System.Collections;

public class RocketAmmo : BaseAmmo {

	public float damageRadius;

	public override void OnTriggerEnter(Collider collider) {

		gameObject.SetActive (false);
		SpawnExplosion ();	//Spawning the set explosion

	}
}

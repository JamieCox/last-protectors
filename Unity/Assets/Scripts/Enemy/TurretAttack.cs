﻿using UnityEngine;
using System.Collections;

public class TurretAttack : MonoBehaviour {

	[Header("Turret Settings")]
	public GameObject turret;
	public Transform bulletSpawn;
	public float detectionRadius = 10f;
	[Range(0.1f, 2f)]
	public float rotDamp = 1f;


	[Header("Weapon Settings")]
	public string pooledBullet;
	public int bulletsPerVolley = 3;
	public float timeBetweenShot = 0.3f;
	public float timeBetweenAttacks = 3f;

	GameObject target;
	int bulletCount;
	float attackTimer;
	float shotTimer;
	bool playerInRange = false;
	

	// Use this for initialization
	void OnEnable () {
		target = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		attackTimer += Time.deltaTime;
		playerInRange = false;

		Collider[] playerCheck = Physics.OverlapSphere (transform.position, detectionRadius);

		foreach (Collider c in playerCheck) {
			if (c.CompareTag("Player"))
				playerInRange = true;
		}

		LookAtTarget ();

		if (attackTimer > timeBetweenAttacks) {
			if (playerInRange)
				Attack ();
			else 
				attackTimer = 0;
		}
	}


	void LookAtTarget() {
		if (target != null) {
			Vector3 lookPos = target.transform.position - turret.transform.position;
			lookPos.y = 0;
			Quaternion newRot = Quaternion.LookRotation (lookPos);
			turret.transform.rotation = Quaternion.Slerp (turret.transform.rotation, newRot, Time.deltaTime * rotDamp);


		}
	}

	void Attack() {
		shotTimer += Time.deltaTime;

		if (shotTimer > timeBetweenShot && bulletCount < bulletsPerVolley) {
			Spawn(bulletSpawn, pooledBullet);

			shotTimer = 0;
			bulletCount ++;

			if (bulletCount == bulletsPerVolley) {
				bulletCount = 0;
				attackTimer = 0;
			}

		}
	}

	public void Spawn(Transform pTransform, string objName) {
		if (objName != "") {
			GameObject g = PoolManager.current.GetPooledObject(objName);
			
			if (g != null) {
				g.transform.position = pTransform.position;
				g.transform.rotation = pTransform.rotation;
				g.SetActive(true);
			}
		}
	}
	
}
﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : BaseAmmo {

	public override void OnTriggerEnter(Collider collider) { 
		if (!collider.CompareTag ("Enemy")) {
			if (collider.CompareTag("Player")) {
				collider.SendMessageUpwards("TakeDamage", baseDamage);
			}
			gameObject.SetActive(false);
			SpawnExplosion();
		}
	}
}

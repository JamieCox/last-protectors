﻿using UnityEngine;
using System.Collections;

public class EnemyAttackClose : MonoBehaviour {

	[Header("Attack Settings")]
	public float timeBetweenAttacks = 0.5f;
	public int attackDamage = 15;

	GameObject player;
	PlayerController playerC;
	bool playerInRange;
	float timer;

	void OnEnable() {
		player = GameObject.FindGameObjectWithTag("Player");
		playerC = player.GetComponent<PlayerController> ();
	}

	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject == player)
			playerInRange = true;
	}

	void OnTriggerExit(Collider collider) {
		if (collider.gameObject == player)
			playerInRange = false;
	}

	void Update() {
		timer += Time.deltaTime;

		if (timer >= timeBetweenAttacks && playerInRange) {
			Attack();
		}
	}

	void Attack() {
		timer = 0f;

		if (playerC.currHealth > 0)
			playerC.TakeDamage(attackDamage);
	}
}

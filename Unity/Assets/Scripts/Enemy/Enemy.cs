﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	[Header("General Stats")]
	public int maxHealth = 20;
	[HideInInspector]
	public int currHealth;
	[HideInInspector]
	public bool isDead = false;

	[Header("Score Values")]
	public float expValue = 50f;
	public int scoreValue = 50;

	[HideInInspector]
	public Transform target;

	private PlayerController player;
	private EnemyUIScript uIScript;
	private WaveManager waveManager;

	// Use this for initialization
	void OnEnable () {
		currHealth = maxHealth;

		uIScript = GetComponent<EnemyUIScript> ();
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
		waveManager = GameObject.Find ("WaveManager").GetComponent<WaveManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (currHealth <= 0)
			Death ();

	}


	public void TakeDamage(int damage) {

		if (currHealth > 0) {
			currHealth -= damage;
		} else if (currHealth <= 0)
			Death ();
	}

	void Death() {
		isDead = true;
		ScoreManager.playerScore += scoreValue;
		player.AddExperience (expValue);
		gameObject.SetActive (false);
		uIScript.healthPanel.SetActive(false);
		waveManager.enemiesStillAlive--;
	}
}

﻿using UnityEngine;
using System.Collections;

public class SeekingAI : MonoBehaviour {

	private PlayerController player;
	private Transform playerTransform;
	private NavMeshAgent nav;

	// Use this for initialization
	void OnEnable () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
		playerTransform = player.transform;
		nav = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		nav.SetDestination (playerTransform.position);
	}
}

﻿using UnityEngine;
using System.Collections;

public class TurtleAttack : MonoBehaviour {

	[Header("Attack Settings")]
	public float timeBetweenAttacks = 5f;
	public int noOfBullets = 8;
	public float angleBetweenBullets = 45f;

	[Header("Attack GameObjects")]
	public string pooledBullet;
	public GameObject bulletSpawnPoint;

	float attackTimer;

	// Update is called once per frame
	void Update () {
		attackTimer += Time.deltaTime;

		if (attackTimer >= timeBetweenAttacks) {
			Attack();
			attackTimer = 0f;
		}
	}

	void Attack() {
		for (int i = 0; i < noOfBullets; i++) {
			float angle = i * angleBetweenBullets - ((angleBetweenBullets / 2) * noOfBullets - 1);
			Quaternion rot = transform.rotation * Quaternion.AngleAxis(angle, Vector3.up);

			Spawn (bulletSpawnPoint.transform, rot, pooledBullet);
		}
	}

	public void Spawn(Transform pTransform, Quaternion pRotation, string objName) {
		if (objName != "") {
			GameObject g = PoolManager.current.GetPooledObject(objName);
			
			if (g != null) {
				g.transform.position = pTransform.position;
				g.transform.rotation = pRotation;
				g.SetActive(true);
			}
		}
	}
}

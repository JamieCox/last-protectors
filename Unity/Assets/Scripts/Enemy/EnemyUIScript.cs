﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyUIScript : MonoBehaviour {

	private Enemy enemyScript;
	private DepthUIScript depthUIScript;

	[Header("Global UI Elements")]
	public Canvas uiCanvas;
	[Tooltip("Place created UI Prefab in here.")]
	public GameObject enemyUIPrefab;

	[Header("UI Settings")]
	[Tooltip("Provides an offset for the prefab in the y-axis.")]
	public float uiOffset = -0.35f;

	[HideInInspector]
	public GameObject healthPanel;

	private Slider healthSlider;
	private Renderer selfRenderer;

	void Awake() {
		enemyScript = GetComponent<Enemy> ();
		uiCanvas = (Canvas)FindObjectOfType (typeof(Canvas));
		healthPanel = Instantiate (enemyUIPrefab) as GameObject;
		healthPanel.transform.SetParent (uiCanvas.transform, false);

		healthSlider = healthPanel.GetComponentInChildren<Slider> ();
		healthSlider.maxValue = enemyScript.maxHealth;

		healthPanel.SetActive (false);

		depthUIScript = healthPanel.GetComponent<DepthUIScript> ();
		//uiCanvas.GetComponent<CanvasScreenSpace> ().AddToCanvas (healthPanel);
	}

	void Update() {
		UpdateState ();
		UpdatePosition ();
	}

	void UpdateState() {

		healthSlider.value = enemyScript.currHealth;

		if (healthSlider.value >= enemyScript.maxHealth)
			healthPanel.SetActive (false);

		else if (healthSlider.value == healthSlider.minValue) {
			healthPanel.SetActive(false);
		}

		else 
			healthPanel.SetActive (true);




	}
	
	void UpdatePosition() {

		Vector3 worldPos = new Vector3 (transform.position.x, transform.position.y + uiOffset, transform.position.z);
		Vector3 screenPos = Camera.main.WorldToScreenPoint (worldPos);
		healthPanel.transform.position = new Vector3 (screenPos.x, screenPos.y, screenPos.z);
		
		float dist = (worldPos - Camera.main.transform.position).magnitude;
		depthUIScript.depth = -dist;


	}
}

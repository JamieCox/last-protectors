﻿using UnityEngine;
using System.Collections;

public class WanderingAI : MonoBehaviour {

	[Header("Wander Settings")]
	public float wanderRadius; 	//Max radius for the new random position
	public float wanderTimer;	//Time between changing destinations 

	private Transform target;	//Varuiable to hold the current position for the wandering target
	private NavMeshAgent agent;	//Reference to the enemies NavMeshAgent
	private float timer;		//Timer
	
	void OnEnable () {
		agent = GetComponent<NavMeshAgent> ();	//Assigning the reference to NavMeshAgent
		timer = wanderTimer;					//Maxing out timer so enemy starts moving as soon as it spawns 
	}

	void Update () {
		timer += Time.deltaTime;	//Increasing the timer

		if (timer >= wanderTimer) {			//Check to see if it's time to move
			Vector3 newPos = RandomWanderPos(transform.position, wanderRadius, -1);	//Find new pos using method
			agent.SetDestination(newPos);	//Setting new destination in NavAgent so enemy will move towards it
			timer = 0;						//Reseting the timer
		}
	}

	public static Vector3 RandomWanderPos(Vector3 origin, float dist, int layermask) {
		Vector3 randDirection = Random.insideUnitSphere * dist;

		randDirection += origin;

		NavMeshHit navHit;

		NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);

		return navHit.position;
	}
}

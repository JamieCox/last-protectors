﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
	[Header("General Stats")]
	[Range(1.0f, 10.0f)]
	public float moveSpeed = 5f;			//Speed of the players movement
	public float maxHealth = 100.0f;
	[HideInInspector]
	public float currHealth;
	private bool isDead;
	private bool damaged;

	[Header("Player Weapon System")]
	/* Variables for the player's equipped guns, and his current gun */
	public Transform gunHold;
	public GameObject currentGun;

	[Tooltip("Select which weapons you want the player to start with.")]
	public GameObject[] guns;

	private int gunIndex = 0;

	[Header("Health UI")]
	/* Vairables for the Players UI */
	public Slider healthSlider;
	public Slider healthSliderBG;
	public Image damageImage;
	public float flashSpeed;
	public Color damageColor = new Color(1f, 0f, 0f, 0.1f);
	private float timeAfterLastDamage = 1f;
	private float timer;

	[Header("Experience UI")]
	/* Values for the leveling system of the player */
	public Slider expSlider;
	public Text levelText;
	public Text levelUI;
	private int level = 0;
	private float currentExp = 0f;
	private float neededExp = 0f;

	/* Various private variables */
	private Vector3 movementVector;			//Holds the players movement input as a vector 
	private Rigidbody playerRB;				//Holds the rigidbody of the player

	private int floorMask;					//Will hold the numerical mask value for the mouse look
	private float mouseRayLength = 100f;	//The lenght of the raycast from the camera so that the player can look at the mouse

	private float vInput;					//Holds the value for the players vertical input
	private float hInput;					//Holds the value for the players horizontal input

	private bool paused;


	void Awake() {
		LevelUp (); //Making sure that the player has an initial level value of 1

		//Creating the layer mask for the floor to interact with the mouse
		floorMask = LayerMask.GetMask ("Floor");

		//Setting up the reference to the players rigidbody
		playerRB = GetComponent<Rigidbody> ();

		//Equiping the first gun in the gun index when the player starts the game
		EquipGun (gunIndex);

		//Setting all of the values for the health system
		currHealth = maxHealth;					//Setting the health to the max health of the player
		healthSlider.maxValue = maxHealth;		//Making sure that the green health bar is set to the players health
		healthSliderBG.maxValue = maxHealth;	//The same as above
		healthSliderBG.value = maxHealth;		//The sane as above
	}

	void FixedUpdate() {
		HandleInput ();
		Movement ();
		AimAtMouse ();
		UpdateUI ();
	}

	void HandleInput() {
		//Getting the movement directions of the player based off of the keys
		vInput = Input.GetAxisRaw("Vertical");
		hInput = Input.GetAxisRaw("Horizontal");

		//Checking to see if the player has pressed the key to change weapon
		if (Input.GetKeyDown(KeyCode.Q)) {
			//Call method to switch weapon, if you couldn't tell by the method name
			SwitchWeapon();
		}
	}

	/* Moving the player */
	void Movement() {
		movementVector.Set (hInput, 0.0f, vInput);

		movementVector = movementVector.normalized * moveSpeed * Time.deltaTime;

		playerRB.MovePosition (playerRB.transform.position + movementVector);
	}

	/* Makes the player aim at the world point the mouse is looking at */
	void AimAtMouse() {
		Ray aimRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit floorPos;

		if (Physics.Raycast (aimRay, out floorPos, mouseRayLength, floorMask)) {
			Vector3 playerAim = floorPos.point - transform.position;

			playerAim.y = 0.0f;

			Quaternion newAim = Quaternion.LookRotation(playerAim);

			playerRB.MoveRotation (newAim);
		}
	}

	/* Method loops through all of the indexes in the gun array */
	void SwitchWeapon() {
		gunIndex++; 					//Increases index by 1 when method is called
		if (gunIndex >= guns.Length)	//Checks to see if new index value is within scope of array
			gunIndex = 0;				//If it is outside array, reset back to first index (0)

		EquipGun (gunIndex);			//Calls equip weapon method at the new index
	}

	/* Method equips the weapon  */
	void EquipGun (int i) {
		if (currentGun) {
			Destroy(currentGun.gameObject);
		}

		currentGun = Instantiate (guns [i], gunHold.position, gunHold.rotation) as GameObject;
		currentGun.transform.parent = gunHold;
	}
	/* Called on weapon pickup. Added gun pickup to the player and replaces the one which was currently equiped. */
	public void EquipNewWeapon(GameObject newGun) {
		guns [gunIndex] = newGun;

		EquipGun(gunIndex);
	}

	/* Updating the player's UI elements */
	void UpdateUI () {
		healthSlider.maxValue = maxHealth;
		healthSliderBG.maxValue = maxHealth;
		healthSlider.value = currHealth;

		if (damaged)
			damageImage.color = damageColor;
		else 
			damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);

		damaged = false;

		expSlider.value = currentExp;
		levelText.text = currentExp + " EXP/" + neededExp + " EXP";
		levelUI.text = "Level: " + level;

		timer += Time.deltaTime;
		if (timer >= timeAfterLastDamage)
			healthSliderBG.value = Mathf.Lerp (healthSliderBG.value, healthSlider.value, 2 * Time.deltaTime);
	}

	public void AddExperience(float exp) {
		currentExp += exp;

		if (currentExp >= neededExp) {
			currentExp -= neededExp;
			LevelUp();
		}
	}

	void LevelUp () {
		level++;
		neededExp = level * 50 + Mathf.Pow (level * 4, 2);
		expSlider.maxValue = neededExp;
		AddExperience (0);
	}

	public void TakeDamage(int amount) {
		damaged = true;

		currHealth -= amount;
	}

	public void AddHealth (int amount) {
		currHealth += amount;
	}
}
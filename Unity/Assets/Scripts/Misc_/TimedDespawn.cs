﻿using UnityEngine;
using System.Collections;

public class TimedDespawn: MonoBehaviour {

	public float lifeTime = 6f;

	private float timer;

	void OnEnable() {
		timer = 0f;
	}

	void Update() {
		timer += Time.deltaTime;

		if (timer >= lifeTime)
			gameObject.SetActive (false);
	}
}

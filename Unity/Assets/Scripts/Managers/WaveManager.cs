﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WaveManager : MonoBehaviour {
	/*===================
	 * Creating a custom data type to easily store wave infromation
	 ====================*/
	[System.Serializable]
	public class Wave {
		public List<Entry> entries;
		
		public Wave() {
			this.entries = new List<Entry>();
		}
		[System.Serializable]
		public class Entry {
			public string enemy;
			public int count;
			[HideInInspector]
			public int spawned;
			
			public Entry(string enemy, int count) {
				this.enemy = enemy;
				this.count = count;
				this.spawned = 0;
			}
		}
	}

	[Header("Waves Setup")]
	public List<Wave> waves = new List<Wave> ();
	//The pooled objects we want to spawn
	public List<string> pooledEnemies = new List<string>();

	[Header("Wave Spawning")]
	//A distance from the view of the camera in which enemies will spawn,
	//making sure that they do not spawn in sight of character
	public float spawnBuffer = 100f;

	[Header("Wave Properties")]
	//Time between each wave
	public float waveSpread = 10f;
	public float spawnTime = 3f;
	public int startWave = 1;
	//Value increases as waves increase so there is an increase in difficulty
	public int startDifficulty = 1;

	[Header("Wave UI")]
	//Reference to UI Text
	public Text waveText;
	//Hiding the number of alive enemies so the player cannot fuck with it via inspector
	[HideInInspector]
	public int enemiesStillAlive = 0;

	//Setting the central spawn pos to 0,0,0
	Vector3 spawnPos = Vector3.zero;
	int waveNumber;
	float timer;
	Wave currWave;
	int spawnThisWave = 0;
	int totalToSpawn;
	bool shouldSpawn = false;
	int difficulty;

	void Start() {
		//Allowing us to start on higher waves
		waveNumber = startWave > 0 ? startWave - 1 : 0;
		difficulty = startDifficulty;

		//Create the Waves 
		//CreateWaves ();

		//Start the first wave
		StartCoroutine("StartNextWave");
	}

	void Update() {
		if (!shouldSpawn) {
			return;
		}

		if (spawnThisWave == totalToSpawn && enemiesStillAlive == 0) {
			StartCoroutine("StartNextWave");
			return;
		}

		timer += Time.deltaTime;

		if (timer >= spawnTime) {
			foreach (Wave.Entry entry in currWave.entries) {
				if (entry.spawned < entry.count)
					Spawn(entry);
			}
		}
	}

	IEnumerator StartNextWave() {
		shouldSpawn = false;

		yield return new WaitForSeconds (waveSpread);

		if (waveNumber == waves.Count) {
			waveNumber = 0;
			difficulty++;
			//waves = new List<Wave>();
			//CreateWaves();
		}

		currWave = waves [waveNumber];

		totalToSpawn = 0;

		foreach (Wave.Entry entry in currWave.entries) {
			totalToSpawn += entry.count;
		}

		spawnThisWave = 0;
		shouldSpawn = true;

		waveNumber++;

//		waveText.text = (waveNumber + (difficulty - 1) * waves.Count).ToString();
	}

	void Spawn(Wave.Entry entry) {
		timer = 0;

		Vector3 randPos = Random.insideUnitSphere * 35;
		randPos.y = 0;

		NavMeshHit hit;;
		if (!NavMesh.SamplePosition (randPos, out hit, 5, 1)) {
			return;
		}

		spawnPos = hit.position;

		Vector3 screenPos = Camera.main.WorldToScreenPoint (spawnPos);
		if ((screenPos.x > -spawnBuffer && screenPos.x < (Screen.width + spawnBuffer)) && (screenPos.y > - spawnBuffer && screenPos.y < (Screen.height + spawnBuffer))) {
			return;
		}
		SpawnEnemy (spawnPos, entry.enemy);

		entry.spawned++;
		spawnThisWave++;
		enemiesStillAlive++;
	}

	public void SpawnEnemy(Vector3 pTransform, string objName) {
		if (objName != "") {
			GameObject g = PoolManager.current.GetPooledObject(objName);
			
			if (g != null) {
				g.transform.position = pTransform;
				g.transform.rotation = Quaternion.identity;
				g.SetActive(true);
			}
		}
	}

	void CreateWaves() {
		Wave wave1 = new Wave();
		wave1.entries.Add(new Wave.Entry(pooledEnemies[0], 10 * difficulty));
		waves.Add(wave1);
		
		Wave wave2 = new Wave();
		wave2.entries.Add(new Wave.Entry(pooledEnemies[0], 10 * difficulty));
		waves.Add(wave2);
		
		Wave wave3 = new Wave();
		wave3.entries.Add(new Wave.Entry(pooledEnemies[0], 10 * difficulty));
		wave3.entries.Add(new Wave.Entry(pooledEnemies[1], 5 * difficulty));
		waves.Add(wave3);
		
		Wave wave4 = new Wave();
		wave4.entries.Add(new Wave.Entry(pooledEnemies[0], 10 * difficulty));
		wave4.entries.Add(new Wave.Entry(pooledEnemies[1], 10 * difficulty));
		waves.Add(wave4);
		
		Wave wave5 = new Wave();
		wave5.entries.Add(new Wave.Entry(pooledEnemies[2], 1 * difficulty));
		waves.Add(wave5);
		
		Wave wave6 = new Wave();
		wave6.entries.Add(new Wave.Entry(pooledEnemies[0], 15 * difficulty));
		wave6.entries.Add(new Wave.Entry(pooledEnemies[1], 15 * difficulty));
		waves.Add(wave6);
		
		Wave wave7 = new Wave();
		wave7.entries.Add(new Wave.Entry(pooledEnemies[0], 15 * difficulty));
		wave7.entries.Add(new Wave.Entry(pooledEnemies[1], 15 * difficulty));
		wave7.entries.Add(new Wave.Entry(pooledEnemies[2], 1 * difficulty));
		waves.Add(wave7);
		
		Wave wave8 = new Wave();
		wave8.entries.Add(new Wave.Entry(pooledEnemies[0], 15 * difficulty));
		wave8.entries.Add(new Wave.Entry(pooledEnemies[1], 15 * difficulty));
		wave8.entries.Add(new Wave.Entry(pooledEnemies[2], 4 * difficulty));
		waves.Add(wave8);
		
		Wave wave9 = new Wave();
		wave9.entries.Add(new Wave.Entry(pooledEnemies[0], 20 * difficulty));
		wave9.entries.Add(new Wave.Entry(pooledEnemies[1], 20 * difficulty));
		wave9.entries.Add(new Wave.Entry(pooledEnemies[2], 4 * difficulty));
		waves.Add(wave9);
		
		Wave wave10 = new Wave();
		wave10.entries.Add(new Wave.Entry(pooledEnemies[0], 20 * difficulty));
		wave10.entries.Add(new Wave.Entry(pooledEnemies[1], 20 * difficulty));
		wave10.entries.Add(new Wave.Entry(pooledEnemies[2], 5 * difficulty));
		waves.Add(wave10);
	}
}
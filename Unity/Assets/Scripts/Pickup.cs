﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	public GameObject weaponPickUp;
	public float healthRegrain;

	public bool isWeaponPickUp = true;
	public int scoreValue;
	public float expValue;

	private PlayerController player;

	void Start() {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ();
	}

	void OnTriggerEnter(Collider collider) {
		if (collider.CompareTag ("Player")) {
			if (isWeaponPickUp)
				collider.SendMessage("EquipNewWeapon", weaponPickUp);
			else
				collider.SendMessage("AddHealth", healthRegrain);

			ScoreManager.playerScore += scoreValue;
			player.AddExperience(expValue);

			Destroy (gameObject);
		}

	}
}

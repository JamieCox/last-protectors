﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CanvasScreenSpace : MonoBehaviour {

	List<DepthUIScript> panels = new List<DepthUIScript>();

	void Awake() {
		panels.Clear ();
	}

	void Update() {
		Sort ();
	}

	public void AddToCanvas(GameObject objToAdd) {
		panels.Add (objToAdd.GetComponent<DepthUIScript> ());
	}

	void Sort() {
		panels.Sort ((x, y) => x.depth.CompareTo(y.depth));
		for (int i = 0; i < panels.Count; i++) {
			panels[i].transform.SetSiblingIndex(i);
		}
	}
}

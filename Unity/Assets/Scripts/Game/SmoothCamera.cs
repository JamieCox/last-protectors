﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour {

	public Transform cameraTarget;				//Target that the camera will follow
	public float movementSmoothing = 5.0f;		//Speed which the camera will follow the target

	private Vector3 cameraOffset;				//The initial position offset of the camera

	void Start() {
		cameraOffset = transform.position - cameraTarget.position;
	}

	void FixedUpdate() {
		//creating a target position for the camera that it will lerp to
		Vector3 targetPosition = cameraTarget.position + cameraOffset;

		//Smoothly move towards the taget pos from the current pos
		transform.position = Vector3.Lerp (transform.position, targetPosition, Time.deltaTime * movementSmoothing);
	}
}
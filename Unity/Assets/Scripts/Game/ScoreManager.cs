﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public Text scoreText;
	public static int playerScore;
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score: " + playerScore;
	}
}
